#!/bin/sh

set -eu

if [ -f "version.txt" ]; then
    VERSION="$(cat version.txt)"
    printf "Preparing to Upload version %s\n" "$VERSION"
    RELEASE="$(curl --fail --silent --show-error "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/releases/v$VERSION")"
    URL="$(printf "%s" "$RELEASE" | jq -r '.assets.links[]|select(.name == "vault-'"$VERSION"'.qcow2").url')"
    printf "Release URL: %s\n" "$URL"
    /app/doctl compute image create "vault-$VERSION" --image-url "$URL" --region nyc3
fi
