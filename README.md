# Vault VM Image

## Create image

```sh
docker run \
    --rm \
    --user "$(id -u):$(id -g)" \
    --volume "$(pwd):/w" \
    --workdir /w \
    registry.gitlab.com/slagit/docker/qemu-builder:1.0.0@sha256:ab266b0aeb8fb40cc1526f07ff2ba08f20be92373005c388641bcca20e94d72f \
    qemu-system-x86_64 \
        -m 1G \
        -nographic \
        -kernel /boot/vmlinuz-virt \
        -initrd /initrd.img \
        -append "console=ttyS0" \
        -virtfs "local,path=/w,mount_tag=host,security_model=mapped-xattr"
```
