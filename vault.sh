#!/bin/sh

set -o errexit
set -o nounset

sed -i -e '/^tty[1-6]:.*$/d' -e '/^ttyS0:/d' /etc/inittab
sed -i -e 's/^root:.*$/root:!::0:::::/' /etc/shadow

cat > /etc/network/interfaces <<-EOF
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1 inet dhcp
EOF

mkdir /etc/udhcpc
cat > /etc/udhcpc/udhcpc.conf <<-EOF
NO_DNS=eth1
EOF

cat > /bin/do-init <<-EOF
#!/bin/sh

set -eu

resize2fs /dev/vda

/usr/local/bin/vault-config

rc-update del do-init default
exit 0
EOF

cat > /etc/init.d/do-init <<-EOF
#!/sbin/openrc-run
depend() {
    need net
}
command="/bin/do-init"
command_args=""
pidfile="/tmp/do-init.pid"
EOF

chmod +x /bin/do-init
chmod +x /etc/init.d/do-init

cat > /etc/iptables/rules-save <<-EOF
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
[0:0] -A INPUT -i lo -j ACCEPT
[0:0] -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
[0:0] -A INPUT -i eth0 -p tcp -m tcp --dport 443 -j ACCEPT
[0:0] -A INPUT -i eth1 -p tcp -m tcp --dport 80 -j ACCEPT
[0:0] -A INPUT -i eth1 -p tcp -m tcp --dport 443 -j ACCEPT
[0:0] -A INPUT -i eth1 -p tcp -m tcp --dport 8201 -j ACCEPT
COMMIT
EOF
chmod 600 /etc/iptables/rules-save

# renovate: datasource=github-releases depName=hashicorp/vault
VAULT_VERSION=1.6.3

addgroup -S vault
adduser -S -h /var/lib/vault -g vault -s /sbin/nologin -G vault -D -H vault
mkdir /etc/vault
mkdir /var/lib/vault
chmod 700 /var/lib/vault
chown -R vault:vault /var/lib/vault
mkdir /var/log/vault
chmod 700 /var/log/vault
chown -R vault:vault /var/log/vault
wget -O /tmp/vault.zip https://releases.hashicorp.com/vault/$VAULT_VERSION/vault_${VAULT_VERSION}_linux_amd64.zip
echo "844adaf632391be41f945143de7dccfa9b39c52a72e8e22a5d6bad9c32404c46  /tmp/vault.zip" | sha256sum -c -
(cd /usr/local/bin && unzip /tmp/vault.zip)
rm /tmp/vault.zip
setcap "cap_ipc_lock=+ep cap_net_bind_service=+ep" /usr/local/bin/vault

cat > /etc/conf.d/vault <<-EOF
# /etc/conf.d/vault

CFGFILE="/etc/vault/config.hcl"

ARGS=""
EOF

cat > /etc/init.d/vault <<-EOF
#!/sbin/openrc-run

command="/usr/local/bin/vault"
description="vault daemon"
extra_started_commands="reload"
pidfile="/var/run/vault/vault.pid"

depend() {
    need net
    after firewall do-init
    provide vault-server
}

reload() {
    ebegin "Reloading vault"
    start-stop-daemon \\
        --pidfile "\${pidfile}" \\
        --quiet \\
        --signal 1
    eend \$? "Failed to reload vault"
}

start() {
    ebegin "Starting vault"
    mkdir -m 0750 -p /var/run/vault
    chown vault:vault /var/run/vault
    chown vault:vault /dev/tty2
    start-stop-daemon \\
        --background \\
        --exec "\${command}" \\
        --group vault \\
        --pidfile "\${pidfile}" \\
        --quiet \\
        --start \\
        --stderr /dev/tty2 \\
        --stdout /dev/tty2 \\
        --user vault \\
        -- server -config "\${CFGFILE}" \${ARGS}
    eend \$? "Failed to start vault"
}

stop() {
    ebegin "Stopping vault"
    start-stop-daemon \\
        --pidfile "\${pidfile}" \\
        --quiet \\
        --stop
    eend \$? "Failed to stop vault"
}
EOF
chmod +x /etc/init.d/vault

cat > /etc/logrotate.d/vault <<-EOF
/var/log/vault/audit.log {
  compress
  daily
  dateext
  dateformat %Y-%m-%d.
  delaycompress
  extension log
  missingok
  postrotate
    /sbin/rc-service vault reload
  endscript
  rotate 30
}
EOF

# renovate: datasource=gitlab-tags depName=slagit/vault/config
VAULT_CONFIG_VERSION=1.2.14

RELEASE_DATA=$(wget -O- https://gitlab.com/api/v4/projects/22217184/releases/v$VAULT_CONFIG_VERSION)
RELEASE_URL=$(printf "%s" "$RELEASE_DATA" | jq -r ".assets.links[] | select(.name==\"vault-config-$VAULT_CONFIG_VERSION-linux-amd64.zip\").direct_asset_url")
wget -O /tmp/vault-config.zip "$RELEASE_URL"
echo "e14d9158715e03c4dd7a3539bcc80bd6a090f097ddf7cea3563e1bc76c16a134  /tmp/vault-config.zip" | sha256sum -c -
(cd /usr/local/bin && unzip /tmp/vault-config.zip)
rm /tmp/vault-config.zip

rc-update add chronyd default
rc-update add crond default
rc-update add do-init default
rc-update add iptables default
rc-update add vault default
