#!/bin/sh

set -eu

sh ./alpine-make-vm-image \
    --branch v3.11 \
    --image-format qcow2 \
    --image-size 512M \
    --mirror-uri https://alpine.global.ssl.fastly.net/alpine/ \
    --packages "chrony e2fsprogs-extra iptables jq logrotate" \
    --script-chroot \
    vault-large.qcow2 ./vault.sh

apk --no-cache --virtual .build-deps add qemu-img
qemu-img convert \
    -O qcow2 \
    -c \
    vault-large.qcow2 \
    vault.qcow2
apk del .build-deps
