#!/bin/sh

set -eu

VERSION=$1

mv "vault.qcow2" "vault-$VERSION.qcow2"
sha256sum "vault-$VERSION.qcow2" > "vault-$VERSION.qcow2.sha256"

printf "%s" "$VERSION" > version.txt
